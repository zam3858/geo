<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function() {
    // semua route dalam blok ini wajib ada token

    Route::apiResource('locations','LocationController');

    Route::get('/', function() {
        return "hello world";
    });

    Route::get('/profile', 'UserController@profile');
    Route::get('/users', 'UserController@index');

});

Route::post('/login', 'UserController@login');

//listkan semua rekod locations
// Route::get('/locations', "LocationController@index");

// //show location tertentu
// Route::get('/locations/{location}', "LocationController@show");

// // tambah rekod location baru
// Route::post('/locations', "LocationController@store");

// // kemaskini rekod location
// Route::put('/locations/{location}', "LocationController@update");

// // delete rekod location
// Route::delete('/locations/{location}', "LocationController@destroy");

// http://geo.test/api/query1
Route::get('/query1', function() {
    // SELECT * FROM locations 
    // WHERE  1=1
    // AND user_id = 1
    // AND id < 20
    // OR name LIKE "%a%"

    $locations = App\Location::where('user_id', '=', 1)
                    ->where('id','<', 20)
                    ->orWhere('name', 'LIKE', '%a%')
                    ->get();
    
    return $locations;

});

// http://geo.test/api/query2
Route::get('/query2', function() {
    // SELECT * FROM locations 
    // ORDER By name ASC

    $locations = App\Location::orderBy('name', 'ASC')
                    ->get();
    
    return $locations;

});

// http://geo.test/api/query3
Route::get('/query3', function() {
    // dapatkan senarai lokasi yang hanya ada pengemaskini

    $locations = App\Location::doesntHave('pengemaskini')
                    ->with('pengemaskini')
                    ->get();
    
    return $locations;

});

// http://geo.test/api/query4
Route::get('/query4', function() {
    // raw query. buat la apa2 sql yang valid

    $locations = DB::select("select * from locations");
    
    return $locations;

});