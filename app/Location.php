<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'locations';
    protected $primaryKey = 'id';

    function pengemaskini() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
