<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    
    public function login() {
        $credential['email'] = request()->email;
        $credential['password'] = request()->password;

        if( \Auth::attempt($credential) ) {
            //berjaya login
            $token = \Auth::user()->createToken('GEO')
                            ->accessToken;
            
            return ['token' => $token];
        } else {
            //tak berjaya
            abort(401);
        }
    }

    public function profile() {
        $user = auth()->user();

        return $user;
    }

    public function index() {
        $users = User::with('locations')->get();

        return $users;
    }

}