<?php

namespace App\Http\Controllers;

use App\Location;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = \App\Location::with('pengemaskini')->get();
    
        return $locations;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        request()->validate([
                'name' => 'required|unique:locations|min:5',
                'longitude' => 'required|numeric',
                'latitude' => 'required|numeric',
            ]);

        //prepare untuk rekod baru
        $location = new \App\Location();

        //isi rekod baru dengan data yang dihantar
        $location->name = request()->name;
        $location->longitude = request()->longitude;
        $location->latitude = request()->latitude;

        //save ke database
        $location->save();

        return $location;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {   
        return $location;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Location $location)
    {

        request()->validate([
            'name' => [ 'required',
                            Rule::unique('locations')->ignore($location->id),
                            'min:5' 
                        ],
            'longitude' => 'required|numeric',
            'latitude' => 'required|numeric',
        ]);

        $location->name = request()->name;
        $location->longitude = request()->longitude;
        $location->latitude = request()->latitude;
        $location->user_id = auth()->user()->id;

        //save ke database
        $location->save();

        return $location;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $location->delete();

        return [ 'status' => true ];
    }
}
